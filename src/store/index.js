import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import { userSession, blockstack } from "boot/blockstack";
import merge from "lodash.merge";

Vue.use(Vuex);

let state = {
  notes: {},
  toolbarHeader: "",
  user: {
    authenticated: false
  }
};

let mutations = {
  SET_NOTES(state, payload) {
    state.notes = payload;
  },
  UPDATE_DAY(state, payload) {
    Object.assign(state.notes[payload.id], payload.note);
  },
  DELETE_DAY(state, day) {
    Vue.delete(state.notes, day);
  },
  ADD_DAY(state, payload) {
    Vue.set(state.notes, payload.id, payload.note);
  },
  SET_HEADER(state, payload) {
    state.toolbarHeader = payload;
  },
  SET_USER(state, payload) {
    Object.assign(state.user, payload);
    // state.user = payload;
  },
  MERGE_NOTES(state, payload) {
    merge(state.notes, payload);
  }
};

let actions = {
  async mergeNotes(context, payload) {
    await context.commit("MERGE_NOTES", payload);
  },
  async setUser(context, payload) {
    await context.commit("SET_USER", payload);
  },
  setNotes(context, payload) {
    context.commit("SET_NOTES", payload);
  },
  updateDay(context, payload) {
    context.commit("UPDATE_DAY", payload);
  },
  deleteDay(context, day) {
    context.commit("DELETE_DAY", day);
  },
  async addDay(context, payload) {
    await context.commit("ADD_DAY", payload);
    context.dispatch("web3Store");
  },
  async web3Store(context) {
    userSession.putFile("notes.json", JSON.stringify(context.state.notes));
  },
  async loadBlockstackData(context) {
    userSession
      .getFile("notes.json")
      .then(data => {
        let notes = JSON.parse(data) || {};
        // context.dispatch("mergeNotes", notes)
        context.dispatch("setNotes", notes);
      })
      .catch(err => {
        console.log("Error loading notes: ", err);
        context.dispatch("setNotes", {});
      });
  },
  async initBlockstack(context) {
    let userData = userSession.loadUserData();
    let name,
      avatar,
      address = "";
    try {
      let profile = await blockstack.lookupProfile(userData.username);
      name = profile.name ? profile.name : "My Account";
      avatar = profile.image[0].contentUrl
        ? profile.image[0].contentUrl
        : "statics/images/mike-j-hover.svg";
      address = userData.username ? userData.username : "unknown address";
    } catch (err) {
      console.log("Error loading profile: ", err);
      name = "My Account";
      avatar = "/statics/images/mike-j-hover.svg";
      address = "unknown address";
    }
    let user = {
      type: "blockstack",
      name: name,
      address: address,
      avatar: avatar,
      authenticated: true
    };
    await context.dispatch("setUser", user);
    context.dispatch("loadBlockstackData");
  }
};

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state,
    actions,
    mutations,
    plugins: [
      createPersistedState({
        key: "HoverLook",
        paths: ["notes"]
      })
    ],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
